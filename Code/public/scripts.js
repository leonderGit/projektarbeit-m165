const recipesContainer = document.getElementById("recipes");

document.getElementById("searchButton").addEventListener("click", () => {
  const ingredient = document.getElementById("ingredientInput").value;

  fetch(`/api/recipes/search?ingredient=${ingredient}`)
    .then((response) => response.json())
    .then((data) => {
      displayRecipes(data);
    })
    .catch((error) => {
      console.error(error);
      recipesContainer.innerHTML = "<p>Error retrieving recipes</p>";
    });
});

function displayRecipes(recipes) {
  recipesContainer.innerHTML = "";
  recipes.forEach((recipe) => {
    const recipeElement = document.createElement("div");
    recipeElement.innerHTML = `
      <h3>${recipe.Rezeptname}</h3>
      <p>Kategorie: ${recipe.Rezeptkategorie}</p>
      <p>Erstelldatum: ${recipe.Erstelldatum}</p>
      <p>Bewertung: ${recipe.Bewertung}</p>
      <p>Kommentar: ${recipe.Kommentar}</p>
      <p>Backzeit: ${recipe.Backzeit}</p>
      <p>Schwierigkeitsgrad: ${recipe.Schwierigkeitsgrad}</p>
      <p>Zutaten: ${recipe.Zutaten.join(", ")}</p>
      <p>Anleitung: ${recipe.Anleitung}</p>
    `;
    // Loop through the attachments and create an <img> element for each one
    for (const attachmentName in recipe._attachments) {
      if (recipe._attachments.hasOwnProperty(attachmentName)) {
        const attachmentUrl = `/api/recipes/attachment?id=${recipe._id}&attachment=${attachmentName}`;
        const imgElement = document.createElement("img");
        imgElement.src = attachmentUrl;
        recipeElement.appendChild(imgElement);
      }
    }

    recipesContainer.appendChild(recipeElement);
  });
}

fetch("/api/recipes")
  .then((response) => response.json())
  .then((data) => {
    displayRecipes(data);
  })
  .catch((error) => {
    console.error(error);
    recipesContainer.innerHTML = "<p>Error retrieving recipes</p>";
  });
