const express = require("express");
const router = express.Router();
const db = require("./db");

router.get("/recipes", (req, res) => {
  db.view("recipes", "all", (err, body) => {
    if (err) {
      console.error(err);
      res.status(500).json({ error: "Internal server error" });
    } else {
      const recipes = body.rows.map((row) => row.value);
      res.json(recipes);
    }
  });
});

module.exports = router;
