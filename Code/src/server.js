const express = require("express");
const path = require("path");
const request = require("request");
const app = express();
const port = 3000;

// CouchDB credentials
const username = "admin";
const password = "admin";
const dbUrl = "http://127.0.0.1:5984/rezepte-leon-gensch"; // Update with your CouchDB URL

app.use(express.static("public"));

// Add a new route handler for ingredient search
app.get("/api/recipes/search", (req, res) => {
  const ingredient = req.query.ingredient;
  const options = {
    url: `${dbUrl}/_design/recipes/_view/by_ingredient?key="${ingredient}"`,
    auth: {
      user: username,
      pass: password,
    },
  };

  request.get(options, (error, response, body) => {
    if (error) {
      console.error(error);
      res.status(500).json({ error: "Internal server error" });
    } else {
      const recipes = JSON.parse(body).rows.map((row) => row.value);
      res.json(recipes);
    }
  });
});

// Add a new route handler for serving image attachments
app.get("/api/recipes/attachment", (req, res) => {
  const id = req.query.id;
  const attachment = req.query.attachment;
  const options = {
    url: `${dbUrl}/${id}/${attachment}`,
    auth: {
      user: username,
      pass: password,
    },
  };

  request.get(options).pipe(res);
});

app.get("/api/recipes", (req, res) => {
  const options = {
    url: `${dbUrl}/_design/recipes/_view/all`,
    auth: {
      user: username,
      pass: password,
    },
  };

  request.get(options, (error, response, body) => {
    if (error) {
      console.error(error);
      res.status(500).json({ error: "Internal server error" });
    } else {
      const recipes = JSON.parse(body).rows.map((row) => row.value);
      res.json(recipes);
    }
  });
});

app.get("*", (req, res) => {
  res.sendFile(path.join(__dirname, "../public/index.html"));
});

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
